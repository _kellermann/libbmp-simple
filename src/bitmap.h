/*!
 * \file	bitmap.h
 * \date	2012-03
 * \ingroup	bmp
 *
 * \author	Darius Kellermann <darius.kellermann@smail.fh-koeln.de>
 *
 * This library implements functions to read and write Windows Bitmap files with
 * a very simple API. It is quite limited in its support for bitmap types
 * though.
 *
 *  Bitmap format version 5 (the most recent) is supported for reading.
 *  Writing is limited to the lowest common denominator format
 *  (v1/BITMAPCOREHEADER). The internal format is an one-dimensional array of
 *  unpadded BGR values.
 *
 *  \warning	Compression is not supported (neither read nor write).
 *		Writing bitdepths <= 8 results in grayscale images, due
 *		to the way the color tables are auto-generated.
 */

#ifndef BITMAP_H_
#define BITMAP_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct px_rgb {
	uint8_t blue;
	uint8_t green;
	uint8_t red;
};

struct px_rgba {
	uint8_t blue;
	uint8_t green;
	uint8_t red;
	uint8_t alpha;
};

/*!
 * Reads a bitmap file into an array of pixels.
 *
 * \warning	This function allocates memory in \p pixels it does not free by
 * 		itself.
 *
 * \param[in]	file		Path to the bitmap file.
 * \param[out]	px_array	Pointer to the array of pixels, type has to be
 * 				determined by the bitdepth. The pointer will be
 * 				allocated, so if it is already a valid pointer,
 * 				free it beforehand. Must be NULL.
 * \param[out]	height		Height of the image.
 * \param[out]	width		Width of the image.
 * \param[out]	bitdepth	Bitdepth of the image.
 *
 * \return	0	Success
 *		1	Can't open file for reading; can't read from file
 *		2	File format not supported
 */
int read_bmp(const char *file, char **px_array, int32_t *height,
		uint32_t *width, uint16_t *bitdepth);

/*!
 * Writes an array of pixels into a bitmap file.
 *
 * \param[in]	file		Path where the bitmap should be saved (no
 * 				checking is performed whatsoever, existing files
 * 				are overwritten without notice).
 * \param[in]	pixels		Pointer to an array of pixels.
 * \param[in]	height		Height of the image in pixels. Positive when the
 * 				image is last row first, else negative.
 * \param[in]	n_cols		Width of the image in pixels.
 * \param[in]	bitdepth	Bitdepth of the image. Possible values are 1, 4,
 * 				8 and 24.
 *
 *  \return	0, when successful, -1 otherwise.
 */
int write_bmp(const char *file, char *px_array, int32_t height,
		uint32_t n_cols, uint16_t bitdepth);

#endif /* BITMAP_H_ */

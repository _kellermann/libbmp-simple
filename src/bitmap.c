/*!
 * \file	bitmap.c
 * \date	2012-03
 * \ingroup	bmp
 *
 * \author	Darius Kellermann <darius.kellermann@smail.fh-koeln.de>
 */

#include "bitmap.h"

#include <errno.h>
#include <math.h>

#pragma pack(1)
typedef struct {
	char magic_no[2];	/*!< Magic number, can be one of (BM, BA, CI, CP, IC, PT). */
	uint32_t file_size;	/*!< Size of the bitmap file in bytes (unreliable). */
	uint16_t reserved0; 	/*!< Reserved; value is application-specific. */
	uint16_t reserved1;	/*!< Reserved; value is application-specific. */
	uint32_t data_offset;	/*!< Image data offset. */
} BITMAPFILEHEADER;

#pragma pack(1)
typedef struct {
	uint32_t header_size;	/*!< Size of this header (12 bytes). */
	int32_t	width;		/*!< Width of the bitmap in pixels. */
	int32_t	height;		/*!< Height of the bitmap in pixels. */
	uint16_t planes;	/*!< Number of color planes, must be 1. */
	uint16_t bitdepth;	/*!< Bitdepth, possible values are: 1, 4, 8 and 24. */
} BITMAPCOREHEADER;

#pragma pack(1)
typedef struct {
	uint32_t header_size;	/*!< Size of this header (40 bytes). */
	int32_t	width;		/*!< Width of the bitmap in pixels. */
	int32_t	height;		/*!< Height of the bitmap in pixels. */
	uint16_t planes;	/*!< Obsolete, must be 1. */
	uint16_t bitdepth;	/*!< Bitdepth, possible values are: 1, 4, 8, 16, 24 and 32. */
	uint32_t compression;	/*!< Compression format, only RGB is supported. */
	uint32_t image_size;	/*!< Size of the image data in bytes. */
	int32_t	h_res;		/*!< Horizontal resolution of the image. */
	int32_t	v_res;		/*!< Vertical resolution of the image. */
	uint32_t clrs_used;	/*!< Number of colors used in the color table (if any). */
	uint32_t clrs_imprtnt;	/*!< Number of important colors; ignored. */
} BITMAPINFOHEADER;

enum BITMAP_COMPRESSIONS {
	BI_RGB = 0,
	BI_RLE8 = 1,
	BI_RLE4 = 2,
	BI_BITFIELDS = 3,
	BI_JPEG = 4,
	BI_PNG = 5,
	BI_ALPHABITFIELDS = 6
};

int read_bmp(const char *file, char **px_array, int32_t *height,
		uint32_t *width, uint16_t *bitdepth)
{
	int result, ret;
	FILE *f_bmp;			/* file pointer for bitmap file */
	size_t read;
	BITMAPFILEHEADER file_hdr;
	BITMAPINFOHEADER info_hdr;	/* bitmap info header */

	result = -1;
	f_bmp = NULL;

	/*
	 * Check parameters.
	 */
	if (*px_array != NULL) {
		errno = EINVAL;
		goto finish;
	}

	f_bmp = fopen(file, "rb");
	if (f_bmp == NULL)
		goto finish;

	read = fread(&file_hdr, sizeof(file_hdr), 1, f_bmp);
	if (read < 1)
		goto finish;

	/*
	 * Check the bitmap file header.
	 */
	if (file_hdr.magic_no != 0x4D42) {
		/*
		 * TODO Determine the correct errno to set in this case.
		 */
	}

	read = fread(&info_hdr, sizeof(info_hdr), 1, f_bmp);
	if (read < 1)
		goto finish;

	if (height != NULL)
		*height = info_hdr.height;
	if (width != NULL)
		*width = info_hdr.width;
	if (bitdepth != NULL)
		*bitdepth = info_hdr.bitdepth;

	*px_array = malloc(info_hdr.image_size);
	if (*px_array == NULL)
		goto finish;

	ret = fseek(f_bmp, file_hdr.data_offset, SEEK_SET);
	if (ret < 0)
		goto finish;

	read = fread(*px_array, info_hdr.image_size, 1, f_bmp);
	if (read < 1)
		goto finish;

	result = 0;

finish: if (result != 0 && *px_array != NULL)
		free(*px_array);

	return result;
}

int write_bmp(const char *file, char *px_array, int32_t height,
		uint32_t n_cols, uint16_t bitdepth)
{
	int result;
	FILE *f_bmp;			/* file pointer for bitmap file */
	size_t written;
	BITMAPFILEHEADER fileheader;	/* file header */
	BITMAPINFOHEADER infoheader;	/* bitmap info header */
	uint32_t i, j;			/* counter variables */
	uint8_t n_colors;		/* number of colors */
	uint32_t *color_table;		/* color table */
	uint32_t color;			/* variable to hold single color */
	uint32_t n_rows;		/* the absolute height of the image */

	result = -1;
	f_bmp = NULL;
	n_colors = 1 << bitdepth;	/* 2^bitdepth */
	color_table = NULL;
	n_rows = abs(height);

	/*
	 * Check parameters.
	 */
	if (bitdepth != 1 && bitdepth != 4 && bitdepth != 8 && bitdepth != 24) {
		errno = EINVAL;
		goto finish;
	}

	/*
	 * Open bitmap file.
	 */
	f_bmp = fopen(file, "wb");
	if (f_bmp == NULL)
		goto finish;

	/*
	 * Assemble and write the file and core headers.
	 */
	fileheader.magic_no = ('M' << 8) | 'B';
	fileheader.data_offset = sizeof(fileheader) + sizeof(infoheader);
	if (bitdepth <= 8) {
		/* A color table is required; its size is 4 * n_colors */
		fileheader.data_offset += 4 * n_colors;
	}
	/* offset + height * number of bytes in row (includes padding) */
	fileheader.file_size = fileheader.data_offset
			+ (n_rows * ((n_cols * bitdepth) / 32) * 4);
	fileheader.reserved0 = 0;
	fileheader.reserved1 = 0;

	infoheader.header_size = sizeof(infoheader);
	infoheader.width = n_cols;
	infoheader.height = height;
	infoheader.planes = 1;
	infoheader.bitdepth = bitdepth;
	infoheader.compression = BI_RGB;
	infoheader.image_size = n_rows * n_cols * (bitdepth / 8);
	infoheader.h_res = 3780;	/* FIXME */
	infoheader.v_res = 3780;
	infoheader.clrs_used = 0;
	infoheader.clrs_important = 0;

	written = fwrite(&fileheader, sizeof(fileheader), 1, f_bmp);
	if (written < 1)
		goto finish;

	written = fwrite(&infoheader, sizeof(infoheader), 1, f_bmp);
	if (written < 1)
		goto finish;

	/*
	 * If required, assemble and write the color table.
	 */
	switch (bitdepth) {
	case 1:
		color_table = malloc(n_colors * sizeof(*color_table));
		color_table[0] = 0x000000;	/* black */
		color_table[1] = 0xFFFFFF;	/* white */
		break;
	case 4:
		color_table = malloc(n_colors * sizeof(*color_table));
		for (i = 0; i < 16; i++)
			color_table[i] = (((i) << 16) | ((i) << 8) | i) & 0xFFFFFF;
		break;
	case 8:
		color_table = malloc(n_colors * sizeof(*color_table));
		for (i = 0; i < 256; i++)
			color_table[i] = (((i) << 16) | ((i) << 8) | i) & 0xFFFFFF;
		break;
	default:
		/* No color table required */
		goto skip_ct;
	}
	written = fwrite(color_table, sizeof(*color_table), n_colors, f_bmp);
	if (written < n_colors)
		goto finish;

skip_ct:
	/*
	 * Write the pixel array to the file.
	 */
	color = 0;	/* for padding */
	if (bitdepth >= 8) {
		for (i = 0; i < n_cols * n_rows; i++) {
			written = fwrite(px_array, 1, bitdepth/8, f_bmp);
			if (written < bitdepth/8)
				goto finish;
			/*
			 * If current byte is last byte is last byte of a row
			 * and number of bytes in one row is not a multiple of
			 * four the line has to be padded up.
			 */
			if (i % n_cols == n_cols - 1 && (n_rows * (bitdepth/8)) % 4 != 0) {
				j = i + 1;
				while (j % 4 != 0) {
					written = fwrite(&color, 1, 1, f_bmp);
					if (written < 1)
						goto finish;
					j++;
				}
			} /* end if padding required */

			px_array += bitdepth/8;
		} /* end for image data */
	} /* end if bitdepth >= 8 */
	else {
		goto finish;
	}

	/* TODO write image data for < 1 byte bitdepths */

	result = 0;

finish: if (f_bmp != NULL)
		fclose(f_bmp);
	free(color_table);

	return result;
}
